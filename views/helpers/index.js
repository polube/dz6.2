
module.exports = (hbs) => {

    hbs.registerHelper('any', function (aString) {
        return new hbs.SafeString("<th>" + aString + "</th>");
    })


    hbs.registerHelper('ncheck', function (number) {
        let numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9,];
        let num;
        let numb = new Array();
        for (let i = 0; i < number.length; i++) {
            let numSt = number;
            numb[i] = "";
            for (let a = 0; a < numSt.length; a++) {
                for (let s = 0; s < numbers.length; s++) {
                    if (numSt.charAt(a).includes(numbers[s])) {
                        // numb.push(num.charAt(a));
                        numb[i] += numSt.charAt(a)
                    }

                }
            }
            num = numb[i];
            if (numb[i].length > 10) {
                return new hbs.SafeString("<th class='bold'>" + number + "</th>");
            } else {
                return new hbs.SafeString("<th>" + number + "</th>");
            }
        }
    })

    hbs.registerHelper('img', function (aString) {
        return new hbs.SafeString("<th><a target='blank' href=" + aString + "><img height='50px' src=" + aString + " alt=''></a></th>");
    })

    hbs.registerHelper('green', function (aString) {
        for (let i = 0; i < aString.length; i++) {
            if (aString === "Microsoft") {
                return new hbs.SafeString("<th class='green'>" + aString + "</th>");
            } else {
                return new hbs.SafeString("<th>" + aString + "</th>");
            }
        }
    })

    return hbs;

}

